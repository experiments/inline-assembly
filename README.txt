Some references:

http://ibiblio.org/gferg/ldp/GCC-Inline-Assembly-HOWTO.html
http://gcc.gnu.org/onlinedocs/gcc-4.8.2/gcc/Extended-Asm.html
http://gcc.gnu.org/onlinedocs/gcc-4.8.2/gcc/Constraints.html
http://drpaulcarter.com/pcasm/

Debugging assembly code with gdb:

gdb ./inline-asm-SIMD-sum
(gdb) break sum
(gdb) run
(gdb) display/i $pc
(gdb) stepi
(gdb) stepi
